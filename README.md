### Promethes & Thanos stack

In this repository is prepared Prometheus & Thanos stack that you can create via docker-compose tool.

#### Manual
1. Clone this project: `git clone git@gitlab.com:wwwaldauf/datascript-prometheus-thanos.git`
2. Open `pt` dir.
3. Run `docker-compose up`.
4. Play! :]
